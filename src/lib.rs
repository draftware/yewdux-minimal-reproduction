use serde::{Deserialize, Serialize};
use yew::prelude::*;
use yewdux::prelude::*;

#[function_component]
pub fn App() -> Html {
    let (store, dispatch) = use_store::<Context>();

    let onclick =
        { Callback::from(move |_| dispatch.reduce_mut(|context| context.toggle ^= true)) };

    html! {
        <input type="button" value={store.toggle.to_string()} {onclick} />
    }
}

#[derive(Default, Clone, PartialEq, Serialize, Deserialize, Store)]
#[store(storage = "local", storage_tab_sync)]
struct Context {
    pub toggle: bool,
}
