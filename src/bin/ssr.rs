use poem::{
    endpoint::StaticFilesEndpoint, get, handler, listener::TcpListener, web::Html, Route, Server,
};
use tokio::fs::read_to_string;
use yew::ServerRenderer;
use yewdux_minimal_reproduction::App;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    Server::new(TcpListener::bind("localhost:3000"))
        .run(
            Route::new()
                .at(
                    r#"/<(hydration)(-.+)?\.(js|wasm)>"#,
                    StaticFilesEndpoint::new("./dist"),
                )
                .at("/*", get(ssr)),
        )
        .await
}

#[handler]
async fn ssr() -> Html<String> {
    let html = read_to_string("./dist/index.html").await.unwrap();
    let (before, after) = html.split_once("<body>").unwrap();

    let rendered = ServerRenderer::<App>::new().render().await;

    Html(format!("{before}<body>{rendered}{after}"))
}
