use yewdux_minimal_reproduction::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
