# Yewdux minimal reproduction

## CSR

```bash
trunk serve
```

Works like a charm.

## SSR

### Run

```bash
trunk build hydration.html
cargo run --bin ssr --features="yew/ssr"
```

### Test

```bash
curl localhost:3000
```

Fails :

```
thread 'prokio-runtime-worker' panicked at 'function not implemented on non-wasm32 targets', /Users/pcoves/.cargo/registry/src/github.com-1ecc6299db9ec823/wasm-bindgen-0.2.84/src/lib.rs:1007:1
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
thread 'tokio-runtime-worker' panicked at 'failed to render application: Canceled', /Users/pcoves/.cargo/registry/src/github.com-1ecc6299db9ec823/yew-0.20.0/src/server_renderer.rs:223:18
```

Commenting `src/lib.rs` at line `12` fixes the run-time error.
But of course, I also loose `Yewdux` interest.
